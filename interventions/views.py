# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime, date
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.exceptions import MethodNotAllowed
from django_filters import rest_framework as filters
from interventions.models import Intervention
from interventions.serializers import InterventionSerializer
from interventions.filters import InterventionFilterSet


class InterventionViewSet(viewsets.ModelViewSet):
    """This is the API entry point to perform the REST
    instructions with the interventions.

    :type queryset: class:`Queryset`
    :param queryset: The default queryset to fetch the interventions.
    :type serializer_class: class:`InterventionSerializer`
    :param serializer_class: The serializer used to convert the queryset to a JSON dictionary.
    :type filter_class: class:`InterventionFilterSet`
    :param filter_class: The filterset used to filter the queryset using the url parameters.
    :type required_fields: Tuple
    :param required_fields: The list of the mandatory fields that an intervention needs to avoid the draft status.
    """

    queryset = Intervention.objects.all()
    serializer_class = InterventionSerializer
    filter_class = InterventionFilterSet
    required_fields = ('label', 'description', 'date', 'serviceman_name', 'place_name')


    def create(self, request):
        """This overrides the base method to inject
        the draft status into the intervention's data

        :type request: WSGIRequest
        :param request: The WSGI request sent by the client.

        :return: A response to the request.
        :rtype: Response or JsonResponse
        """

        request.data['is_draft'] = self.get_draft_status(request.data)
        return super(viewsets.ModelViewSet, self).create(request)
    

    def update(self, request, pk=None):
        """This overrides the base method to check that
        the request has not be forged to modify the
        wrong fields and then update the draft status

        :type request: WSGIRequest
        :param request: The WSGI request sent by the client.
        :type pk: int
        :param pk: The id of the intervention.
        

        :return: A response to the request.
        :rtype: Response or JsonResponse
        """

        intervention = Intervention.objects.get(pk=pk)

        if intervention.date is not None:
            form_date = request.data['date']
            date = datetime.strptime(form_date, '%Y-%m-%d').date()

            if date < date.today():
                return Response(
                    'An expired intervention cannot be modified.',
                    status=status.HTTP_400_BAD_REQUEST,
                )

        if request.data.has_key('is_draft') and intervention.is_draft != request.data['is_draft']:
            return Response(
                'The draft status cannot be modified manually.',
                status=status.HTTP_400_BAD_REQUEST,
            )
    
        request.data['is_draft'] = self.get_draft_status(request.data)
        return super(viewsets.ModelViewSet, self).update(request, pk)

    
    def partial_update(self, request, pk=None):
        """This Disables the PATCH requests."""

        raise MethodNotAllowed('PATCH', detail='Method "PATCH" not allowed.')


    def get_draft_status(self, data):
        """This checks all the fields of the intervention's
        data to determine whether it is a draft or not

        :type data: Dictionary
        :param data: Data of the request made to create or update an intervention.

        :return: The draft status of the intervention.
        :rtype: Bool
        """

        for field_name in self.required_fields:
            if field_name not in data or data[field_name] == '':
                return True
        return False
