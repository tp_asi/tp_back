# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from tp_back.settings import LABEL_CHOICE_LENGTH, LABEL_MIN_LENGTH, LABEL_MAX_LENGTH, TEXT_LENGTH


class Intervention(models.Model):
    """This is the model which represents the intervention
    object to be translated in the SQL database.

    :param label: Label of the itervention, used to display its title.
    :param description: Used to define the intervention.
    :param serviceman_name: It is the name of the person who realizes the intervention.
    :param place_name: Geographic place of the intervention.
    :param is_draft: Flag used to check whether the intervention is a draft or not.
    :param date: Date of the intervention realization.
    """

    label = models.CharField(max_length=LABEL_MIN_LENGTH, null=True, blank=True)
    description = models.TextField(max_length=TEXT_LENGTH, null=True, blank=True)
    serviceman_name = models.CharField(max_length=LABEL_MIN_LENGTH, null=True, blank=True)
    place_name = models.CharField(max_length=LABEL_MAX_LENGTH, null=True, blank=True)
    is_draft = models.BooleanField(default=True, null=False)
    date = models.DateField(null=True, blank=True)
