from rest_framework import routers
from django.conf.urls import url, include
from interventions import views

router = routers.SimpleRouter(trailing_slash=False)

router.register(r'interventions-view/?', views.InterventionViewSet, base_name='interventions-view')

urlpatterns = [
    url('', include(router.urls)),
]