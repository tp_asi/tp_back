from django_filters import rest_framework as filters
from interventions.models import Intervention


class InterventionFilterSet(filters.FilterSet):
    """This class will apply filters based on the parameters it
    catches on the URL intercepted by the :class:`ViewSet` it is linked to. You must
    declare it as the :param:`filter_class` attribute of a :class:`ViewSet` to use it.

    :type date: class:`CharFilter`
    :param date: Define filter parameter used to filter the date.
    """

    date = filters.CharFilter(field_name='date', method='order_by_date')


    def order_by_date(self, qs, name, value):
        """It orders the queryset by ascending or descending dates.
        If the parameter value is mispelled, no ordering is done at all.

        :type qs: class:`Queryset`
        :param qs: Define filter parameter used to filter the date.
        :type name: str
        :param name: The name of the parameter.
        :type value: str
        :param value: The value of the parameter.

        :return: The ordered queryset.
        :rtype: class:`Queryset`
        """

        if value == 'ascending':
            return qs.order_by('date')
        elif value == 'descending':
            return qs.order_by('-date')
        return qs


    class Meta:
        model = Intervention
        fields = ['is_draft', 'date']