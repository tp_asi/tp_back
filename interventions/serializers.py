from rest_framework import serializers
from interventions.models import Intervention


class InterventionSerializer(serializers.ModelSerializer):
    """This is the serializer that converts the interventions from
    python objects to JSON. It must be set as the :param:`serializer_class`
    of a :class:`ViewSet` in order to be used.
    """
    
    class Meta:
        model = Intervention
        fields = ('__all__')
