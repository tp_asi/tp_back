# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from datetime import datetime
from freezegun import freeze_time
from interventions.models import Intervention
from interventions.views import InterventionViewSet


class InterventionTest(TestCase):
    
    @freeze_time('2021-07-02')
    def test_getList(self):
        Intervention.objects.create(
            label='intervention 1',
            description='first intervention',
            serviceman_name='repair man 1',
            place_name='place 1',
            is_draft=False,
            date=datetime.today(),
        )

        factory = APIClient()
        response = factory.get('/interventions/interventions-view/')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)


    @freeze_time('2021-07-02')
    def test_getIntervention(self):
        Intervention.objects.create(
            id=0,
            label='intervention 1',
            description='first intervention',
            serviceman_name='repair man 1',
            place_name='place 1',
            is_draft=False,
            date=datetime.today(),
        )
        Intervention.objects.create(
            id=1,
            label='intervention 2',
            description='second intervention',
            serviceman_name='repair man 2',
            is_draft=True,
            date=datetime.today(),
        )

        factory = APIClient()
        response1 = factory.get('/interventions/interventions-view/0')
        response2 = factory.get('/interventions/interventions-view/5')

        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 404)
        self.assertEqual(response1.data['label'], 'intervention 1')


    @freeze_time('2021-07-02')
    def test_createIntervention(self):
        intervention = {
            'label': 'intervention 3',
            'description': 'third intervention',
            'serviceman_name': 'repair man 3',
            'place_name': 'place 3',
            'date' : '2021-7-1',
        }
        intervention_draft = {}

        factory = APIClient()
        response1 = factory.post('/interventions/interventions-view/', intervention, format='json')
        response2 = factory.post('/interventions/interventions-view/', intervention_draft, format='json')

        self.assertEqual(response1.status_code, 201)
        self.assertEqual(response1.data['is_draft'], False)
        self.assertEqual(response2.status_code, 201)
        self.assertEqual(response2.data['is_draft'], True)


    @freeze_time('2021-07-02')
    def test_updateIntervention(self):
        Intervention.objects.create(
            id=0,
            label='intervention 1',
            description='first intervention',
            serviceman_name='repair man 1',
            is_draft=False,
            date=datetime.today(),
        )
        Intervention.objects.create(
            id=1,
            label='intervention 2',
            description='second intervention',
            serviceman_name='repair man 2',
            place_name='place 1',
            is_draft=False,
            date=datetime(2021, 6, 25).date(),
        )
        
        new_intervention1 = {
            'label': 'intervention 3',
            'description': 'third intervention',
            'serviceman_name': 'repair man 3',
            'place_name': 'place 3',
            'date' : '2021-7-5',
        }
        new_intervention2 = {
            'label': 'intervention 4',
            'description': 'third intervention',
            'serviceman_name': 'repair man 4',
            'place_name': 'place 4',
            'date' : '2021-7-1',
        }
        new_intervention3 = {
            'label': 'intervention 4',
            'description': 'third intervention',
            'serviceman_name': 'repair man 4',
            'place_name': 'place 4',
            'is_draft': True,
            'date' : '2021-7-5',
        }

        factory = APIClient()

        response1 = factory.put('/interventions/interventions-view/0', new_intervention1, format='json')
        response2 = factory.put('/interventions/interventions-view/1', new_intervention2, format='json')
        response3 = factory.put('/interventions/interventions-view/0', new_intervention3, format='json')

        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response1.data['is_draft'], False)
        self.assertEqual(response2.status_code, 400)
        self.assertEqual(response3.status_code, 400)
