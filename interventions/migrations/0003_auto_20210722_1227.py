# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-07-22 12:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('interventions', '0002_auto_20210722_1217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='intervention',
            name='date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
