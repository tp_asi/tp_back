# TP_FRONT

Back end part of the intervention application.

## RUNNING THE DEVELOPMENT WEB SERVER

### REQUIREMENTS

- Python 2.7
- Pip
- Git
- A command line interface

### STEPS

- Use the following command to clone the repository `git clone https://gitlab.com/tp_asi/tp_back.git`
- Go inside the directory with `cd tp_back`
- Run the following commands `pip install -r requirements.txt`, `python manage.py runserver` (You might want to run these in your Python Venv if you use one).

The application is now available at : `http://localhost:8000`

### RUNNING THE TESTS

You can run the tests using the following command : `python manage.py test`
